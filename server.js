const express = require('express');
const app = express();
const axios = require("axios");
const { MongoClient } = require('mongodb');
const { log } = require('console');

app.use(express.json())

const mongoUrl = 'mongodb+srv://franco:2310@cluster0.ofoqb.mongodb.net/Cluster0?retryWrites=true&w=majority'

const client = new MongoClient(mongoUrl);

app.post('/', async (req,res) => {
    await client.connect();
    const database = client.db('usuarios');  
    const collection = database.collection("personas");

    const action = req.body.queryResult.action;
    switch(action){
        case "Usuario":
            const persona = req.body.queryResult.parameters;
              const result = await collection.insertOne(persona);
              res.json({
                  fulfillmentText: `Guardado con exito ${persona.nombre}!`
              });
      /*case "Cifras":
          axios.get('https://api.covid19api.com/summary').then(respuesta => {
              const numero_casos = respuesta.data.Global.TotalConfirmed;
              res.json({
                  fulfillmentText: `El total de casos es ${numero_casos}`
              });
          });
          break;
          case "pais":
            const { pais } = req.body.queryResult.parameters;
            axios.get(`https://api.covid19api.com/summary`).then(respuesta => {
                const paises = respuesta.data.Countries;
                const pais_resultado = paises.filter(elem => {
                    return elem.Country == pais
                })
                res.json({
                    fulfillmentText: `El número total de casos para ${pais} es de: ${pais_resultado[0].TotalConfirmed}`
              });
          });
          break;
          case "test":
              const persona = req.body.queryResult.parameters;
              const result = await collection.insertOne(persona);
              res.json({
                  fulfillmentText: `Guardado con exito ${persona.nombre}!`
              });
              break;*/
          default:
                res.json({
                    fulfillmentText: 'No entiendo nada'
                });
                break;

          };  
            
    
    
    /*const {numero1,numero2} = req.body.queryResult.parameters;
    const resultado = numero1 + numero2;
    res.json({
        fulfillmentText: `El resultado de la suma es ${resultado}`
    });*/ 
})
app.get("/datos", async (req, res) => {
    await client.connect();
  
    const database = client.db("usuarios");
    const collection = database.collection("personas");
    const personas = await collection.find({}).toArray();
    console.log(personas);
    res.json({ personas: personas });
  });

app.listen(8080)